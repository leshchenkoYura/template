package example.com.example.presentation.activity.activity_c;

import example.com.example.presentation.base.BasePresenter;
import example.com.example.presentation.base.BaseActivityView;

public interface ICView {
    interface ActivityView extends BaseActivityView {

    }

    interface Presenter extends BasePresenter<ActivityView>{
    }
}
