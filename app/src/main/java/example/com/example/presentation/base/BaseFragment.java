package example.com.example.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment<Binding extends ViewDataBinding> extends Fragment {
    private Binding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
       initView();
       return binding.getRoot();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        attachFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
        startFragment();
    }

    @Override
    public void onDestroy() {
       if (getPresenter() != null){
           getPresenter().onStopView();
       }
        super.onDestroy();
    }

    protected abstract BasePresenter getPresenter();

    protected abstract void initView();

    protected abstract void attachFragment();

    protected abstract void startFragment();

    protected abstract void stopFragment();

    protected abstract void destroyFragment();

    protected abstract void pauseFragment();

    protected abstract void resume();


    @LayoutRes
    protected abstract int getLayoutRes();

    protected Binding getBinding() {
        return binding;
    }

    protected void toastShort(String message){
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    protected void toastLong(String message){
        Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();
    }
}
