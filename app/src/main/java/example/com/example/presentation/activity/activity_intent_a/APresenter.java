package example.com.example.presentation.activity.activity_intent_a;

import example.com.example.R;
import example.com.example.data.Data;
import example.com.example.presentation.activity.activity_intent_b.BActivity;

public class APresenter implements IAView.Presenter {
    private IAView.View view;

    @Override
    public void onStartView(IAView.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        if (view != null) view = null;
    }

    @Override
    public void onClick() {
        Data data = new Data();
        data.setId(R.drawable.ic_baseline);
        view.transactionActivity(BActivity.class,data);
    }
}
