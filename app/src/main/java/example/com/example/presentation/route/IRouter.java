package example.com.example.presentation.route;

import example.com.example.data.Data;
import example.com.example.presentation.base.BaseActivityView;
import example.com.example.presentation.base.DialogCallBack;

public interface IRouter {
    void startView(BaseActivityView view);

    void stopView();

    void step(String cmd, Data data);

    void stepDialog(String cmd, DialogCallBack callBack, Data data);

    void sendData(String tag,Data data);

    void stopStart();

    boolean isStart();
}
