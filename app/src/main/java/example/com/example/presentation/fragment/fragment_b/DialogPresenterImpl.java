package example.com.example.presentation.fragment.fragment_b;

import example.com.example.data.Data;
import example.com.example.presentation.base.DialogCallBack;

public class DialogPresenterImpl implements Dialog.Presenter{
    private Dialog.View view;
    private DialogCallBack callBack;
    private Data data;

    @Override
    public void onStartView(Dialog.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
    }

    @Override
    public void init(Data data, DialogCallBack dialog) {
        callBack = dialog;
        if (view != null && data != null && data.getMessage() != null) {
            this.data = data;
            view.show(data.getMessage());
        }
    }

    @Override
    public void onClickOk() {
        data.setMessage("output Dialog".concat("\t").concat(data.getMessage()));
        if (view != null){
            callBack.sendData(data);
            view.dismissDialog();
        }
    }

    @Override
    public void onClickCancel() {
        if (view != null){
            view.dismissDialog();
        }
    }
}
