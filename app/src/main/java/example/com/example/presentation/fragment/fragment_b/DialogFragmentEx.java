package example.com.example.presentation.fragment.fragment_b;

import android.os.Bundle;
import example.com.example.R;
import example.com.example.data.Data;
import example.com.example.databinding.DialogFragmentBinding;
import example.com.example.presentation.base.BaseDialogFragment;
import example.com.example.presentation.base.BasePresenter;
import example.com.example.presentation.base.DialogCallBack;

public class DialogFragmentEx extends BaseDialogFragment<DialogFragmentBinding>
implements Dialog.View{
    private DialogCallBack  dialog;
    public static final String TAG_DATA = "tag_data";
    private Dialog.Presenter presenter;

    public static DialogFragmentEx newInstance(Data data) {
        DialogFragmentEx fragment = new DialogFragmentEx();
        Bundle args = new Bundle();
        args.putParcelable(TAG_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }
    public void initListener(DialogCallBack dialog){
        this.dialog = dialog;
    }


    @Override
    protected void initDialogView() {
        presenter = new DialogPresenterImpl();
        getBinding().setEvent(presenter);
        if (getArguments() != null){
            Data data = getArguments().getParcelable(TAG_DATA);
            if (data != null){
                presenter.onStartView(this);
                presenter.init(data,dialog);
            }else{
                //TODO error data
            }
        }
    }

    @Override
    protected void createDialog() {

    }

    @Override
    protected void detachFragment() {

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_fragment;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void show(String msg) {
        if (msg != null) {
            getBinding().tvDialogShow.setText(msg);
        }else {
            getBinding().tvDialogShow.setText(R.string.no_data);
        }
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

    @Override
    public void onError(int id) {
        toast(getString(R.string.no_data));
    }


}