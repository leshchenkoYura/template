package example.com.example.presentation.fragment.fragment_b;

import example.com.example.data.Data;
import example.com.example.presentation.base.BasePresenter;
import example.com.example.presentation.base.DialogCallBack;

public interface Dialog {
    interface View{
        void show(String msg);

        void dismissDialog();

        void onError(int msg);
    }

    interface Presenter extends BasePresenter<View> {
        void init(Data data, DialogCallBack dialog);
        void onClickOk();
        void onClickCancel();
    }
}
