package example.com.example.presentation.fragment.fragment_a;

import java.util.concurrent.TimeUnit;

import example.com.example.data.Data;
import example.com.example.data.Timer;
import example.com.example.presentation.base.DialogCallBack;
import example.com.example.presentation.fragment.fragment_b.DialogFragmentEx;
import example.com.example.presentation.route.Route;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class APresenter implements IAContract.Presenter, DialogCallBack {
    private IAContract.View view;
    private Data data;
    private Disposable disposable;

    public APresenter() {
    }

    @Override
    public void onStartView(IAContract.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
       if (disposable != null && !disposable.isDisposed()) {
           disposable.dispose();
       }
       disposable = null;
       view = null;
       data = null;
    }

    @Override
    public void initData(Data data) {
        if (data != null) {
            this.data = data;
            if (view != null)view.show(data.getMessage());
        }
        Timer.getInstance().clearTimer();
        Timer.getInstance().setStartTime(System.currentTimeMillis());
        Timer.getInstance().setRunning(true);
       disposable =  Observable.interval(1_000, TimeUnit.MILLISECONDS, Schedulers.io())
                .flatMap(v ->  Timer.getInstance().run())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(v -> {
                   view.timeShow(v);
               }, e -> {
                   view.onError(e.getMessage());
               });


    }

    @Override
    public void onClickDialog() {
        Route.getInstance().stepDialog(DialogFragmentEx.class.getSimpleName(),this,data);

    }

    @Override
    public void sendData(Data data) {
        view.show(data.getMessage());
    }
}
