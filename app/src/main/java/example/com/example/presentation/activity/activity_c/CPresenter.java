package example.com.example.presentation.activity.activity_c;

import java.util.Date;

import example.com.example.Const;
import example.com.example.data.Data;
import example.com.example.presentation.route.Route;

public class CPresenter implements ICView.Presenter {
    private ICView.ActivityView view;

    @Override
    public void onStartView(ICView.ActivityView view) {
        this.view = view;
        Route.getInstance().step(Const.AFRAGMENT_STEP,new Data(1,"Hello World"));
    }

    @Override
    public void onStopView() {
        if (view != null) view = null;
    }
}
