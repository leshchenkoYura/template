package example.com.example.presentation.activity.activity_c;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import example.com.example.R;
import example.com.example.data.Data;
import example.com.example.databinding.ActivityCBinding;
import example.com.example.presentation.base.BaseActivity;
import example.com.example.presentation.base.BasePresenter;
import example.com.example.presentation.route.Route;
import timber.log.Timber;

public class CActivity extends BaseActivity<ActivityCBinding>
implements ICView.ActivityView {
    private ICView.Presenter presenter;

    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new CPresenter();
        Route.getInstance().startView(this);
        getBinding().setEvent(presenter);
    }

    @Override
    public void onBackPressed() {
//        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
//            if (fragment instanceof AFragment) {
//             //TODO логика при onBackPressed на фрагмент
//            }
//
//        }
        Timber.e("fragment  size ".concat(String.valueOf(getSupportFragmentManager().getFragments().size())));
        if (getSupportFragmentManager().getFragments().size() == 0){
           this.finish();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_c;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);
    }

    @Override
    public void transactionFragmentNoBackStack(Fragment fragment, int container) {
        super.transactionFragmentNoBackStack(fragment,container);
    }

    @Override
    public void transactionFragmentWithBackStack(Fragment fragment, int container) {
        super.transactionFragmentWithBackStack(fragment,container);
    }

    @Override
    public void transactionFragmentDialog(DialogFragment fragment, int container) {
        super.transactionFragmentDialog(fragment,container);
    }

    @Override
    public void closeFragmentDialog(DialogFragment fragment) {
        super.closeFragmentDialog(fragment);
    }

    @Override
    public void sendData(String tag, Data data) {
        super.sendDataBase(tag,data);
    }
}