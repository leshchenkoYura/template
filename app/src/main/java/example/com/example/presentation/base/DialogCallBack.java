package example.com.example.presentation.base;

import example.com.example.data.Data;

public interface DialogCallBack {
    void sendData(Data data);
}
