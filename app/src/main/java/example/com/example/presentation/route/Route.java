package example.com.example.presentation.route;

import javax.security.auth.callback.Callback;

import example.com.example.Const;
import example.com.example.R;
import example.com.example.data.Data;
import example.com.example.presentation.base.BaseActivityView;
import example.com.example.presentation.base.BaseDialogFragment;
import example.com.example.presentation.base.DialogCallBack;
import example.com.example.presentation.fragment.fragment_a.AFragment;
import example.com.example.presentation.fragment.fragment_b.DialogFragmentEx;

public class Route implements IRouter{
    private static IRouter instance;
    private BaseActivityView view;
    private boolean flag = true;
    private BaseDialogFragment dialogFragment;


    private Route() {
    }

    public static synchronized IRouter getInstance(){
            if (instance == null){
                instance = new Route();
            }
            return instance;
    }

    @Override
    public void startView(BaseActivityView view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        view = null;
    }

    @Override
    public void step(String cmd, Data data) {
        switch (cmd){
            case Const.AFRAGMENT_STEP :
                if (view != null) view.transactionFragmentWithBackStack(AFragment.newInstance(data), R.id.content);
            break;
        }
    }

    @Override
    public void stepDialog(String cmd, DialogCallBack callBack, Data data) {
        if (view != null){
            if (cmd.equals(DialogFragmentEx.class.getSimpleName())){
                DialogFragmentEx dialog = DialogFragmentEx.newInstance(data);
                dialog.initListener(callBack);
                view.transactionFragmentDialog(dialogFragment = dialog,R.id.content);
            }
        }
    }

    @Override
    public void sendData(String tag, Data data) {
        if (view != null){
            view.sendData(tag,data);
            view.closeFragmentDialog(dialogFragment);
        }

    }


    @Override
    public void stopStart() {
         flag = false;
    }

    @Override
    public boolean isStart() {
        return flag;
    }

}
