package example.com.example.presentation.base;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import example.com.example.data.Data;

public interface BaseActivityView {
    void transactionFragmentNoBackStack(Fragment fragment, int container);

    void transactionFragmentWithBackStack(Fragment fragment, int container);

    void transactionFragmentDialog(DialogFragment fragment, int container);

    void closeFragmentDialog(DialogFragment fragment);

    void sendData(String tag,Data data);

}
