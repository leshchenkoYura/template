package example.com.example.presentation.fragment.fragment_a;


import android.os.Bundle;
import example.com.example.R;
import example.com.example.data.Data;
import example.com.example.databinding.FragmentABinding;
import example.com.example.presentation.base.BaseFragment;
import example.com.example.presentation.base.BasePresenter;


public class AFragment extends BaseFragment<FragmentABinding> implements IAContract.View {
    private IAContract.Presenter presenter;

    private static final String TAG_DATA = "data";


    public AFragment() {

    }

    public static AFragment newInstance(Data data) {
        AFragment fragment = new AFragment();
        Bundle args = new Bundle();
        args.putParcelable(TAG_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void initView() {
        presenter = new APresenter();
        getBinding().setEvent(presenter);
        if (getArguments() != null){
            presenter.initData(getArguments().getParcelable(TAG_DATA));
            getBinding().tvE.setText(getArguments().getString(TAG_DATA) != null ? getArguments().getString(TAG_DATA) : "no data");
        }
    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.onStartView(this);
    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {
        presenter = null;
    }

    @Override
    protected void pauseFragment() {

    }

    @Override
    protected void resume() {

    }

    public void sendData(Data data){
        presenter.initData(data);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_a;
    }

    @Override
    public void show(String str) {
        getBinding().textView.setText(str);
    }

    @Override
    public void timeShow(String time) {
        if (time != null && !time.isEmpty()){
            getBinding().tvE.setText(time);
        }
    }

    @Override
    public void onError(String msg) {
        toastLong(msg);
    }

}