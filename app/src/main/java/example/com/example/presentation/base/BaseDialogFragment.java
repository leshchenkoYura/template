package example.com.example.presentation.base;

import android.app.Dialog;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;

public abstract class BaseDialogFragment<Binding extends ViewDataBinding> extends DialogFragment {
    private Binding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        initDialogView();
        return binding.getRoot();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        createDialog();
        return super.onCreateDialog(savedInstanceState);
    }

    protected abstract void initDialogView();

    protected abstract void createDialog();

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    public void onDetach() {
        if (getPresenter()!= null){
            getPresenter().onStopView();
        }
        detachFragment();
        super.onDetach();
    }

    protected abstract void detachFragment();

    @LayoutRes
    protected abstract int getLayoutRes();

    protected Binding getBinding() {
        return binding;
    }

    protected void toast(String message){
        Toast.makeText(getActivity(),message, Toast.LENGTH_SHORT).show();
    }


    protected abstract BasePresenter getPresenter();



}
