package example.com.example.presentation.activity.activity_intent_b;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;

import example.com.example.R;
import example.com.example.data.Data;
import example.com.example.databinding.ActivityBBinding;
import example.com.example.presentation.base.BaseActivity;
import example.com.example.presentation.base.BasePresenter;

import static example.com.example.Const.TAG_DATA;

public class BActivity extends BaseActivity<ActivityBBinding>
implements IBView.View{
    private IBView.Presenter presenter;


    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new BPresenter();
        getBinding().setEvent(presenter);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_b;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        Intent i = getIntent();
        Data data = i.getParcelableExtra(TAG_DATA);
        if (data != null){
            getBinding().ivAvatar.setBackground(ContextCompat.getDrawable(this,data.getId()));
        }
    }
}