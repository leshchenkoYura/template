package example.com.example.presentation.activity.activity_calc;

import example.com.example.presentation.base.BasePresenter;

public interface ICalcView {
    interface View{
       void eventButton(String msg);

       void result(String res);
    }
    interface Presenter extends BasePresenter<View> {
        void onClick();

        void onClick(String btn);
    }
}
