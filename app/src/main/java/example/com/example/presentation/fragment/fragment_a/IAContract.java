package example.com.example.presentation.fragment.fragment_a;

import example.com.example.data.Data;
import example.com.example.presentation.base.BasePresenter;

public interface IAContract {
    interface View{
        void show(String str);

        void timeShow(String time);

        void onError(String msg);
    }

    interface Presenter extends BasePresenter<View>{
        void initData(Data data);

        void onClickDialog();
    }
}
