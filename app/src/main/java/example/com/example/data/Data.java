package example.com.example.data;

import android.os.Parcel;
import android.os.Parcelable;


public class Data  implements Parcelable{
    private int id;
    private String message;


    public Data( ) {
    }

    public Data(int id, String message) {
        this.id = id;
        this.message = message;
    }

    protected Data(Parcel in) {
        id = in.readInt();
        message = in.readString();
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(message);
    }
}
