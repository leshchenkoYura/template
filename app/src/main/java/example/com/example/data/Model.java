package example.com.example.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Model implements Parcelable {
    private int id;
    private String name;

    protected Model(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator<Model> CREATOR = new Creator<Model>() {
        @Override
        public Model createFromParcel(Parcel in) {
            return new Model(in);
        }

        @Override
        public Model[] newArray(int size) {
            return new Model[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
    }
}
