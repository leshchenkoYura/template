package example.com.example.app;

import android.app.Application;
import android.content.IntentFilter;

import example.com.example.BuildConfig;
import example.com.example.data.WifiStateReceiver;
import timber.log.Timber;


public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        final IntentFilter filters = new IntentFilter();
        filters.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filters.addAction("android.net.wifi.STATE_CHANGE");
        registerReceiver(new WifiStateReceiver(), filters);
    }
}
